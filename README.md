# Build C-like Compiler

Building language compiler for a language like C (4 projects for phases in language compilation: lexical, syntactic, semantic analysis, intermediate code generation). Implementation in Java.

Detailed project tasks specifications in "TaskSpecification.pdf".

Team project: 4 members.

My contribution: part of code in each of the 4 projects.

Project duration: Sep 2019 - Feb 2020.
